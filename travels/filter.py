from django import forms
from django.db import models
import django_filters

from .models import Travel, TravelCategory

class TravelFilter(django_filters.FilterSet):
    n = django_filters.CharFilter(lookup_expr='icontains', field_name='nama')
    k = django_filters.ModelChoiceFilter(queryset=TravelCategory.objects.all(), field_name='kategori')
    c = django_filters.DateFromToRangeFilter(field_name='dibuat_pada')
    class Meta:
        model = Travel
        fields = ['n', 'k', 'c']
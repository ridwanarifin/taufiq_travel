# Generated by Django 2.2 on 2019-09-27 12:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0003_auto_20190927_1847'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blog',
            options={'ordering': ('-id',)},
        ),
    ]
